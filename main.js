var express = require("express");
var app = express();

var mysql = require("mysql");
const pool = mysql.createPool({
    host: "173.194.233.16",
    user: "fred",
    password: "25 heng mui keng",
    database: "sakila",
    connectionLimit: 3
});

var bodyParser = require("body-parser");

const q = require("q");

var getMovie = function(pool, film_id) {
    var defer = q.defer();
    pool.getConnection(function(err, conn) {
        if (err) {
            defer.reject(err);
            return;
        }
        try {
            conn.query("select * from film where film_id = ?", [film_id],
                function(err, rows) {
                    if (err) {
                        defer.reject(err);
                        return;
                    }
                    if (rows.length)
                        defer.resolve(rows[0]);
                    else

                        defer.reject(404);
                });
        } catch (ex) {
            defer.reject(ex);
        } finally {
            conn.release();
        }
        
    })
    return (defer.promise);
}.bind(undefined, pool);
        
var getMovies = function(pool, opt) {
    var defer = q.defer();
    pool.getConnection(function(err, conn) {
        if (err) {
            defer.reject(err);
            return;
        }
        try {
            opt = opt || { count: 30, start: 0 };
            conn.query("select film_id, title, rating from film limit ? offset ?",
                [ opt.count || 30 , opt.start || 0],
                function(err, rows) {
                    if (err) {
                        defer.reject(err);
                        return;
                    }
                    defer.resolve(rows);
                });
        } catch (ex) { defer.reject(ex); } 
        finally { conn.release(); }
    });
    return (defer.promise);
}


//Process requrest, specify the rules you are using
//to handle a request

app.use(bodyParser.urlencoded({extended: false}));

app.get("/api/film/:film_id", function(req, res) {
    getMovie(req.params.film_id)
        .then(function(rec) {
            res.json(rec);
            
        }).catch(function(err) {
            if (err == 404) {
                res.status(404);
                res.type("text/plain");
                res.send("Not found");
            } else {
                res.status(400);
                res.type("text/plain");
                res.send(err);
            }
        })
});

// application/x-www-form-urlencoded
app.post("/api/film", function(req, res) {
    getMovie(req.body.text)
        .then(function(rec) {
            res.json({ text: rec.title + ": " + rec.rating});
        }).catch(function(err) {
            res.json({ text: "Error: " + err});
        })
})

app.get("/api/films", function(req, res) {
    getMovies(pool)
        .then(function(rows) {
            //defer.resolve
            //res.status(200);
            //res.type("application/json");
            //res.send(rows);
            res.json(rows);
        }).catch(function(err) {
            //defer.reject
            res.status(400);
            res.type("text/plain");
            res.send("error: " + err);
        });
});

app.use("/bower_components", 
    express.static(__dirname + "/bower_components"));

app.use(express.static(__dirname + "/public"));

//Handle file not found - 400
app.use(function(req, res, next) {
    res.status(404);
    res.type("text/plain");
    res.send("File not found: " + req.originalUrl);
});

//Error - 500
app.use(function(err, req, res, next) {
   console.error("Application error: %s", err); 
});

app.set("port", process.env.APP_PORT || 3000);
app.listen(app.get("port"), function() {
    console.info("Application started on port %d", app.get("port"));
});













