(function() {
    var IMDBApp = angular.module("IMDBApp", ["ui.router"]);
    
    var ListCtrl = function($http, $state) {
        var vm = this;
        vm.films = [];
        vm.displayDetails = function(film_id) {
            $state.go("details", 
                { filmId: film_id });
        }
        
        $http.get("/api/films")
            .then(function (result) {
                vm.films = result.data;
                
            }).catch(function(err) {
                console.error(">> %s", err)
            });
    };
    var DetailsCtrl = function($http, $state, $stateParams) {
        var vm = this;
        vm.film = {};
        vm.back = function() {
            $state.go("list");
        }
        console.info(">> film = " + $stateParams.filmId)
        $http.get("/api/film/" + $stateParams.filmId)
            .then(function(result) {
                console.info(">>> returned " + result.data)
                vm.film = result.data;
                
            }).catch(function(err) {
                console.error(">> error = %s", err);
            })
    }
    
    var IMDBConfig = function($stateProvider, $urlRouterProvider) {
        
        $stateProvider.state("list", {
            url: "/list",
            templateUrl: "/views/list.html",
            controller: ["$http", "$state", ListCtrl],
            controllerAs: "listCtrl" 
            
        }).state("details", {
            url: "/film/:filmId",
            templateUrl: "/views/details.html",
            controller: ["$http", "$state", "$stateParams", DetailsCtrl],
            controllerAs: "detailsCtrl"
        });
        
        $urlRouterProvider.otherwise("/list");
    }
    
    IMDBApp.config(["$stateProvider", "$urlRouterProvider", IMDBConfig]);
})();